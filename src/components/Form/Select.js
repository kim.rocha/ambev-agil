import React from 'react';
import {
    View, Text, StyleSheet
} from 'react-native';

import { Picker } from '@react-native-community/picker'

const styles = StyleSheet.create({
    mainView: {
        borderColor: '#4B4B4B',
        borderWidth: 1,
        borderRadius: 5,
        margin: 2

    },
    titleView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: '#4B4B4B',
        borderBottomWidth: 1
    },
    titleText: {
        paddingLeft: 2,
        fontWeight: 'bold'
    },
    errorText: {
        paddingRight: 2,
        color: 'red',
        fontSize: 10,
        justifyContent: 'flex-end',
        textAlignVertical: 'center'
    }

});


const Select = ({
    errors,
    touched,
    title,
    fieldName,
    options,
    value,
    handleChange,
}) => {

    return (
        <View style={styles.mainView}>
            <View style={styles.titleView}>
                <Text style={styles.titleText}>{title}</Text>
                {(errors[fieldName] && touched[fieldName]) && (
                    <Text style={styles.errorText}>{errors[fieldName]}</Text>)
                }
            </View>
            <View>
                <Picker
                    selectedValue={String(value)}
                                        onValueChange={(itemValue) => handleChange(itemValue)}
                >
                    {
                        options.map((option) => (
                            <Picker.Item key={option.value} label={option.label} value={option.value} />
                        ))
                    }
                </Picker>
            </View>
        </View>
    )
}

export default Select