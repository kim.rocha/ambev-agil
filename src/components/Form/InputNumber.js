import React from 'react';
import {
    View, Text, TextInput, StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
    mainView: {
        borderColor: '#4B4B4B',
        borderWidth: 1,
        borderRadius: 5,
        margin: 2

    },
    titleView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: '#4B4B4B',
        borderBottomWidth: 1
    },
    titleText: {
        paddingLeft: 2,
        fontWeight: 'bold'
    },
    errorText: {
        paddingRight: 2,
        color: 'red',
        fontSize: 10,
        justifyContent: 'flex-end',
        textAlignVertical: 'center'
    }

});


const InputNumber = ({
    errors,
    touched,
    title,
    fieldName,
    value,
    placeholder,
    align,
    readOnly = false,
    handleChange,
    handleBlur
}) => {

    return (
        <View style={styles.mainView}>
            <View style={styles.titleView}>
                <Text style={styles.titleText}>{title}</Text>
                {(errors[fieldName] && touched[fieldName]) && (
                    <Text style={styles.errorText}>{errors[fieldName]}</Text>)
                }
            </View>
            <View>
                <TextInput
                    editable={!readOnly}
                    keyboardType={'numeric'}
                    autoCompleteType={'off'}
                    placeholder={placeholder}
                    style={{ textAlign: align }}
                    onChangeText={handleChange}
                    onBlur={handleBlur}
                    value={String(value)}
                />
            </View>
        </View>
    )
}

export default InputNumber