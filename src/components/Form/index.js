import InputNumber from './InputNumber'
import Switch from './Switch'
import BinaryInput from './BinaryInput'
import Select from './Select'

export {
    InputNumber,
    Switch,
    BinaryInput,
    Select
}