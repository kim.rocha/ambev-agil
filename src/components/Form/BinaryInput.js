import React from 'react';
import {
    View, Text, Button, StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
    mainView: {
        borderColor: '#4B4B4B',
        borderWidth: 1,
        borderRadius: 5,
        margin: 2

    },
    titleView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: '#4B4B4B',
        borderBottomWidth: 1
    },
    titleText: {
        paddingLeft: 2,
        fontWeight: 'bold'
    },
    errorText: {
        paddingRight: 2,
        color: 'red',
        fontSize: 10,
        justifyContent: 'flex-end',
        textAlignVertical: 'center'
    },
    buttonsView: {
        flexDirection: 'row',
        marginHorizontal: 10,
        marginVertical: 10,
        justifyContent: 'center'
    },
    buttonView: {
        width: '50%'
    }

});


const BinaryInput = ({
    errors,
    touched,
    title,
    fieldName,
    texts = ['Sim', 'Não'],
    value = true,
    handleChange,
}) => {

    return (
        <View style={styles.mainView}>
            <View style={styles.titleView}>
                <Text style={styles.titleText}>{title}</Text>
                {(errors[fieldName] && touched[fieldName]) && (
                    <Text style={styles.errorText}>{errors[fieldName]}</Text>)
                }
            </View>
            <View style={styles.buttonsView}>
                <View style={styles.buttonView}>
                    <Button onPress={() => handleChange(true)} color={value ? '#00448C' : '#B4C7E7'} title={texts[0]} />
                </View>
                <View style={styles.buttonView}>
                    <Button onPress={() => handleChange(false)} color={!value ? '#00448C' : '#B4C7E7'} title={texts[1]} />
                </View>
            </View>
        </View>
    )
}

export default BinaryInput