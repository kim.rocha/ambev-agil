import React from 'react';
import {
    Button,
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
    button: {
        color: '#00448C'

    }

});


const ButtonPrimary = ({
    title,
    handlePress,
    color = '#00448C'
}) => {

    return (
        <Button onPress={handlePress} color={color} style={styles.button} title={title} />
    )
}

export default ButtonPrimary