
const parseToDefaultNumber = (value) => (!value || value === '') ? value : Number(value.replace(/\./g, '').replace(',', '.'))
const formatNumber = (value, decimalCase = 2) => (!value || value === '') ? value : String(value.toFixed(decimalCase)).replace(/\./g, ',')

const formatCurrency = (value) => (!value || value === '') ? value : `R$ ${formatNumber(value)}`


export {
    parseToDefaultNumber,
    formatNumber,
    formatCurrency
}