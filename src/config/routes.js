import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'
import Core from '../modules/Core'
import Negotiation from '../modules/Negotiation'
import NegotiationSend from '../modules/Negotiation/NegotiationSend'





const navigator = createStackNavigator({
    Core: { screen: Core },
    Negotiation: { screen: Negotiation },
    NegotiationSend: {screen: NegotiationSend}
})


export default createAppContainer(navigator)