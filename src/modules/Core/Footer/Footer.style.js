import {
    StyleSheet,
    Dimensions
} from 'react-native'

const styles = StyleSheet.create({
    footerView: {
        height: 30,
        alignSelf: 'stretch',
        backgroundColor: '#00448C',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',

    },
    footerText: {
        color: 'white',
        fontSize: 12,
    }

});


export default styles