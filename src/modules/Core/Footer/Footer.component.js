import React from 'react'

import {
    View, Text, Image
} from 'react-native'

import styles from './Footer.style'

const Footer = () => {
    return (
        <View style={styles.footerView}>
            <Text
                style={styles.footerText}
            >
                Ambev Ágil | CDD Maceió
            </Text>
        </View>
    )
}

export default Footer


