import {
    StyleSheet
} from 'react-native'

const styles = StyleSheet.create({
    headerView: {
        height: 200,
        backgroundColor: '#00448C',
        alignSelf: 'stretch',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    logo: {
        height: 150,
        resizeMode: 'contain',
    },
    agilText: {
        color: 'white',
        fontSize: 20,
    }
    
});


export default styles