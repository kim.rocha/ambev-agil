import React from 'react'

import {
    View, Text, Image
} from 'react-native'

import styles from './Header.style'

const Header = () => {
    return (
        <View style={styles.headerView}>
            <Image
                style={styles.logo}
                source={require('../../../assets/logo/logo-ambev.png')}
            />
        </View>
    )
}

export default Header


