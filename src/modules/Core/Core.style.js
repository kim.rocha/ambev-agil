import {
    StyleSheet
} from 'react-native'

const styles = StyleSheet.create({
    mainView: {
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
});

export default styles