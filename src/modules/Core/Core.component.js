import React from 'react';
import {
    View
} from 'react-native';
import styles from './Core.style'

import Header from './Header'
import Applications from './Applications'


const Core = ({ navigation }) => (
    <View style={styles.mainView}>
        <Header />
        <Applications navigation={navigation} />
    </View>
)

Core.navigationOptions = () => ({
    headerShown: false
})

export default Core