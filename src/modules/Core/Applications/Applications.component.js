import React from 'react'

import {
    View, Text, Image, FlatList
} from 'react-native'

import styles from './Applications.style'
import { Card, WingBlank, Button } from '@ant-design/react-native'
import ApplicationList from './Application.list'





const Application = ({
    navigation,
    header = {
        title,
        thumb,
        component
    },
    content,
    footer = {
        content,
        extra
    }
}) => {
    return (
        <View style={{ paddingTop: 15 }}>
            <WingBlank size='lg'>
                <Card>
                    <Card.Header
                        title={<Text style={styles.title}>{header.title}</Text>}
                        thumbStyle={{ width: 30, height: 30 }}
                        thumb={<Image style={styles.logo} source={header.thumb} />}
                        extra={<Button style={styles.pushButton} onPress={() => navigation.push(header.component)}>
                            <Text>Ir</Text>
                        </Button>}
                    />
                    <Card.Body>
                        <View style={{ height: 42 }}>
                            <Text style={{ marginLeft: 16 }}>{content}</Text>
                        </View>
                    </Card.Body>
                    <Card.Footer
                        content={footer.content}
                        extra={`ID: ${footer.extra}`}
                    />
                </Card>
            </WingBlank>
        </View>
    )
}

const Applications = ({ navigation }) => {


    return (
        <>
            <FlatList
                style={styles.listView}
                data={ApplicationList}
                renderItem={({ item: application }) =>
                    <Application
                        navigation={navigation}
                        header={{ title: application.name, thumb: application.logo, component: application.component }}
                        content={application.description}
                        footer={{ content: application.maintaner, extra: application.maintanerId }}

                    />}
                ListFooterComponent={<View style={{ height: 50 }}></View>}

            />
        </>

    )
}









export default Applications


