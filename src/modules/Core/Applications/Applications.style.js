import {
    Dimensions,
    StyleSheet
} from 'react-native'

const styles = StyleSheet.create({
    listView: {
        backgroundColor: '#D9D9D9',
        alignSelf: 'stretch',
        height: Dimensions.get('window').height - 200 
    },
    logo: {
        height: 30,
        width: 30,
        resizeMode: 'contain',
        marginRight: 4
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    pushButton:{
        fontSize: 12,
        alignSelf: 'flex-end',
        

    },
    
});


export default styles