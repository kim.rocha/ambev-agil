import React from 'react'



const list = [{
    key: 'negotiation',
    name: 'Negociação Ágil',
    logo: require('../../../assets/icons/agreement.png'),
    description: 'Aplicação para otimização do fluxo de negociação de valores inadimplentes.',
    component: 'Negotiation',
    maintaner: 'Larissa Brito',
    maintanerId: '99748768'
}]

export default list