const creditMatrix = [{
    floor: 0,
    ceil: 15,
    tax: 0,
    penalty: 0,
    discount: 0,
    monthlyInstallment: 0,
    weeklyInstallment: 0
},
{
    floor: 16,
    ceil: 45,
    tax: 0,
    penalty: 0,
    discount: 0,
    monthlyInstallment: 1,
    weeklyInstallment: 4
},
{
    floor: 46,
    ceil: 60,
    tax: 0,
    penalty: 0,
    discount: 0,
    monthlyInstallment: 1,
    weeklyInstallment: 4
},
{
    floor: 61,
    ceil: 90,
    tax: 0,
    penalty: 0,
    discount: 0,
    monthlyInstallment: 1,
    weeklyInstallment: 4
},
{
    floor: 91,
    ceil: 180,
    tax: 0,
    penalty: 0,
    discount: 0.1,
    monthlyInstallment: 6,
    weeklyInstallment: 15
},
{
    floor: 181,
    ceil: 99999999999999999,
    tax: 0,
    penalty: 0,
    discount: 0.2,
    monthlyInstallment: 6,
    weeklyInstallment: 30
}]


export default creditMatrix