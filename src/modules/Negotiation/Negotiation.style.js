import {
    StyleSheet,
    Dimensions
} from 'react-native'

const styles = StyleSheet.create({
    titleView: {
        height: 60,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#00448C'
    },
    titleText: {
        fontSize: 24,
        fontWeight: 'bold',
        color: 'white',
        display: 'flex',
        textAlignVertical: 'center',
    },
    formView: {
        marginHorizontal: 20,
        margin: 10
    },
    submitView: {
        marginTop: 10,
        flexDirection: 'row-reverse'
    },
    simulateView: {
        marginTop: 10,
        marginHorizontal: 20,
    },
    table: {
        alignSelf: 'stretch',
        borderWidth: 1,
        borderRadius: 5,
        marginHorizontal: 20,
    },
    header: {
        height: 30,
        fontSize: 18,
        borderBottomWidth: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
        fontWeight: 'bold'
    },
    row: {
        paddingHorizontal: 5, 
        height: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    label: {
        fontWeight: 'bold'

    },
    discount:{
        color:'green'
    }

});

export default styles