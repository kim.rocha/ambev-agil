import React, { useState } from 'react';
import {
    View, Text, Alert
} from 'react-native';
import styles from './NegotiationSend.style'
import { Formik } from 'formik'
import { InputNumber, BinaryInput, Select } from '../../../components/Form'
import { ButtonPrimary } from '../../../components/Button'
import { parseToDefaultNumber, formatCurrency } from '../../../helpers/NumberFormatHelper'
import * as Yup from 'yup'
import { captureScreen } from "react-native-view-shot";

const calculateQuota = ({
    finalValue,
    numberOfInstallments
}) => {

    if (!numberOfInstallments || numberOfInstallments === 0) return formatCurrency(finalValue)
    const quota = finalValue / numberOfInstallments
    return formatCurrency(quota)
}




const initialValues = ({
    finalValue,
    maxMonthlyInstallment
}) => ({
    code: '',
    monthlyInstallment: true,
    numberOfInstallments: maxMonthlyInstallment,
    quota: calculateQuota({ finalValue, numberOfInstallments: maxMonthlyInstallment })
})


const schema = Yup.object().shape({
    code: Yup.number()
        .required('O código do cliente não pode ser vazio.')
        .integer('O código do cliente deve ser um número inteiro.')
        .positive('O código do cliente não pode ser menor que 0')
        .typeError('O código do cliente deve ser um número.'),
    monthlyInstallment: Yup.boolean()
        .required('O tipo de parcelamento não pode ser vazio.'),
    numberOfInstallments: Yup.number()
        .required('O número de parcelas não pode ser vazio.')
        .integer('O número de parcelas deve ser um número inteiro.')
        .positive('O número de parcelas não pode ser menor que 1.')
        .typeError('O número de parcelas deve ser um número.'),
    quota: Yup.string()
        .required('O valor da parcela não pode ser vazio.')
})


const NegotiationSend = ({
    navigation
}) => {

    const [warning, setWarning] = useState(false)

    const {
        finalValue,
        monthlyInstallment: maxMonthlyInstallment,
        weeklyInstallment: maxWeeklyInstallment
    } = navigation.state.params


    const mountArrayOfInstallments = ({
        monthlyInstallment
    }) => {

        const max = monthlyInstallment ? maxMonthlyInstallment : maxWeeklyInstallment

        let installments = [1];

        while (installments[installments.length - 1] < max) {
            installments.push(installments[installments.length - 1] + 1)
        }

        return installments.map((installment) => ({ value: String(installment), label: String(installment) }))


    }

    const printScreen = () => {
        captureScreen({
            format: "jpg",
            quality: 0.8
        }).then(
            uri => Alert.alert('Salvamento realizado', 'Print salvo com sucesso.'),
            error => Alert.alert('Não foi possível salvar.', 'Tire um print manualmente.')
        );
    }







    return (
        <View>
            <View style={styles.titleView}>
                <Text style={styles.titleText}>Negociação Ágil</Text>
            </View>
            <View>
                <Formik
                    validationSchema={schema}
                    initialValues={initialValues({
                        finalValue,
                        maxMonthlyInstallment
                    })}
                    onSubmit={() => { setWarning(true) }}
                >
                    {({ errors, touched, setFieldValue, handleSubmit, values }) => (
                        <View style={styles.formView}>
                            <InputNumber
                                errors={errors}
                                touched={touched}
                                fieldName={'code'}
                                placeholder={'Insira o código do cliente.'}
                                title={'Código do cliente'}
                                align={'right'}
                                handleChange={(e) => { setWarning(false); setFieldValue('code', parseToDefaultNumber(e)) }}
                                value={values.code}
                            />
                            <BinaryInput
                                errors={errors}
                                touched={touched}
                                fieldName={'monthlyInstallment'}
                                title={'Tipo de parcelamento'}
                                texts={['Mensal', 'Semanal']}
                                handleChange={(e) => {
                                    if (e) {
                                        setFieldValue('numberOfInstallments', maxMonthlyInstallment)
                                        setFieldValue('quota', calculateQuota({ finalValue, numberOfInstallments: maxMonthlyInstallment }));
                                    } else {
                                        setFieldValue('numberOfInstallments', maxWeeklyInstallment)
                                        setFieldValue('quota', calculateQuota({ finalValue, numberOfInstallments: maxWeeklyInstallment }));
                                    }
                                    setWarning(false);
                                    setFieldValue('monthlyInstallment', e)
                                }}
                                value={values.monthlyInstallment}
                            />
                            <Select
                                errors={errors}
                                touched={touched}
                                fieldName={'numberOfInstallments'}
                                title={'Número de parcelas'}
                                handleChange={(e) => {
                                    setWarning(false);
                                    setFieldValue('numberOfInstallments', parseToDefaultNumber(e));
                                    setFieldValue('quota', calculateQuota({ finalValue, numberOfInstallments: parseToDefaultNumber(e) }));
                                }}
                                value={values.numberOfInstallments}
                                options={mountArrayOfInstallments(values)}
                            />
                            <InputNumber
                                errors={errors}
                                touched={touched}
                                fieldName={'quota'}
                                readOnly={true}
                                title={'Valor da parcela'}
                                align={'right'}
                                value={values.quota}
                            />
                            <View style={styles.simulateView}>
                                <ButtonPrimary color="green" handlePress={handleSubmit} title="Finalizar negociação" />
                            </View>
                            <Text style={styles.warning}>{`${warning ? 'Tire o print dessa tela e mostre no retorno de rota para o financeiro.' : ''}`}</Text>
                        </View>
                    )}
                </Formik>
            </View>
        </View>
    )
}


NegotiationSend.navigationOptions = () => ({
    headerShown: false
})

export default NegotiationSend