import React, { useState } from 'react';
import {
    View, Text, TextInput, Button
} from 'react-native';
import styles from './Negotiation.style'
import matrix from './Negotiation.logic'
import { Formik } from 'formik'
import { InputNumber } from '../../components/Form'
import { ButtonPrimary } from '../../components/Button'
import { parseToDefaultNumber, formatCurrency } from '../../helpers/NumberFormatHelper'
import * as Yup from 'yup'


const initialValues = {
    value: '',
    aging: ''
}

const schema = Yup.object().shape({
    value: Yup.number()
        .required('O valor total não pode ser vazio.')
        .positive('O valor devido não pode ser maior que R$ 0,00.')
        .typeError('O valor deve ser um número.'),
    aging: Yup.number()
        .required('O tempo de atraso não pode ser vazio.')
        .integer('O tempo de atraso deve ser um número inteiro.')
        .positive('O tempo de atraso deve ser maior que zero.')
        .typeError('O tempo de atraso deve ser um número.')
})


const Negotiation = ({
    navigation
}) => {

    const [result, setResult] = useState(undefined)

    const calculateResult = ({
        value, aging
    }) => {
        const selectedRow = matrix.find((row) => (aging >= row.floor && aging <= row.ceil))


        const finalValue = value * (1 - selectedRow.discount)

        return {
            aging,
            finalValue,
            discount: selectedRow.discount,
            discountValue: value - finalValue,
            monthlyValue: selectedRow.monthlyInstallment > 0 ? finalValue / selectedRow.monthlyInstallment : '',
            monthlyInstallment: selectedRow.monthlyInstallment,
            weeklyValue: selectedRow.weeklyInstallment > 0 ? finalValue / selectedRow.weeklyInstallment : '',
            weeklyInstallment: selectedRow.weeklyInstallment
        }

    }

    return (
        <View>
            <View style={styles.titleView}>
                <Text style={styles.titleText}>Negociação Ágil</Text>
            </View>
            <View>
                <Formik
                    validationSchema={schema}
                    initialValues={initialValues}
                    onSubmit={values => setResult(calculateResult(values))}
                >
                    {({ errors, touched, setFieldValue, handleSubmit, values }) => (
                        <View style={styles.formView}>
                            <InputNumber
                                errors={errors}
                                touched={touched}
                                fieldName={'value'}
                                placeholder={'Insira o valor total da pendência em atraso.'}
                                title={'Valor devido (R$)'}
                                align={'right'}
                                handleChange={(e) => { setResult(undefined); setFieldValue('value', parseToDefaultNumber(e)) }}
                                value={values.value}
                            />
                            <InputNumber
                                errors={errors}
                                touched={touched}
                                fieldName={'aging'}
                                placeholder={'Insira a quantidade de dias em atraso.'}
                                title={'Dias em atraso'}
                                align={'right'}
                                handleChange={(e) => { setResult(undefined); setFieldValue('aging', parseToDefaultNumber(e)) }}
                                value={values.aging}
                            />
                            <View style={styles.submitView}>
                                <ButtonPrimary handlePress={handleSubmit} title="Ver condições" />
                            </View>
                        </View>
                    )}
                </Formik>
            </View>
            <View>
                {result && (
                    <>
                        <View style={styles.table}>
                            <Text style={styles.header}>Condições</Text>
                            <View style={styles.row}>
                                <Text style={styles.label}>Dias em atraso</Text>
                                <Text style={styles.value}>{`${result.aging} ${result.aging > 1 ? 'dias' : 'dia'}`}</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.label}>Desconto</Text>
                                <Text style={result.discount > 0 ? styles.discount : styles.vlaue}>{`${(result.discount * 100).toFixed(0)}% (${formatCurrency(result.discountValue)})`}</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.label}>Valor a pagar</Text>
                                <Text style={styles.value}>{`${formatCurrency(result.finalValue)}`}</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.label}>Parc. mínima (mensal)</Text>
                                <Text style={styles.value}>{result.monthlyInstallment ? `${result.monthlyInstallment}x de ${formatCurrency(result.monthlyValue)}` : '-'}</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.label}>Parc. mínima (semanal)</Text>
                                <Text style={styles.value}>{result.weeklyInstallment ? `${result.weeklyInstallment}x de ${formatCurrency(result.weeklyValue)}` : '-'}</Text>
                            </View>
                        </View>
                        {result.monthlyInstallment > 0 && (
                            <View style={styles.simulateView}>
                                <ButtonPrimary handlePress={() => navigation.push('NegotiationSend', result)} color="green" title="Quero pagar parcelado!" />
                            </View>
                        )}
                    </>
                )}
            </View>
        </View>
    )
}


Negotiation.navigationOptions = () => ({
    headerShown: false
})

export default Negotiation